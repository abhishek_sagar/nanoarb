package main

import (
	"errors"
	"fmt"
	"io"
	"nanoarb/auth"
	"net/http"
	"os"
	"strings"
)

type InputType string
type Operation string

const (
	Image     InputType = "image"
	Text      InputType = "text"
	Audio     InputType = "audio"
	Video     InputType = "video"
	Numerical InputType = "numerical"
	Series    InputType = "series"
)

const (
	Categorization  Operation = "categorization"
	Transcription   Operation = "transcription"
	ImageSynthesis  Operation = "image_synthesis"
	AudioSynthesis  Operation = "audio_synthesis"
	ObjectDetection Operation = "object_detection"
	TextExtraction  Operation = "text_extraction"
	Scoring         Operation = "scoring"
	Similarity      Operation = "similarity"
	Summarization   Operation = "summarization"
)

type inputSpec struct {
	Must []InputType
	Can  []InputType
}

var OpMap map[Operation]inputSpec
var OpMetadataMap map[Operation]OpMetadata

type OpMetadata struct {
	Title string `json:"title"`
	Desc  string `json:"desc"`
	Image string `json:"image"`
}

//This should ideally moved into some datastore
func initVars() {
	OpMap = map[Operation]inputSpec{}
	OpMap[Categorization] = inputSpec{
		Must: []InputType{Image},
		Can:  []InputType{Image},
	}
	OpMap[Transcription] = inputSpec{
		Can: []InputType{Video, Audio},
	}
	OpMap[ImageSynthesis] = inputSpec{
		Can: []InputType{Text},
	}
	OpMap[AudioSynthesis] = inputSpec{
		Can: []InputType{Text},
	}
	OpMap[ObjectDetection] = inputSpec{
		Can: []InputType{Image, Video},
	}
	OpMap[TextExtraction] = inputSpec{
		Must: []InputType{Text},
		Can:  []InputType{Text},
	}
	OpMap[Scoring] = inputSpec{
		Can: []InputType{Image, Text},
	}
	OpMap[Similarity] = inputSpec{
		Can: []InputType{Image, Text, Audio, Video},
	}
	OpMap[Summarization] = inputSpec{
		Can: []InputType{Text, Audio, Audio},
	}

	OpMetadataMap = map[Operation]OpMetadata{}
	OpMetadataMap[Categorization] = OpMetadata{
		Title: "Categorization",
		Desc:  "Use this to split your data into multiple categories",
		Image: "https://nanonets.com/some/image/link",
	}
	OpMetadataMap[Transcription] = OpMetadata{
		Title: "Transcription",
		Desc:  "Generate text from an audio or video file",
		Image: "https://nanonets.com/some/image/link",
	}
	OpMetadataMap[ImageSynthesis] = OpMetadata{
		Title: "Image synthesis",
		Desc:  "Generate an image from text",
		Image: "https://nanonets.com/some/image/link",
	}
	OpMetadataMap[AudioSynthesis] = OpMetadata{
		Title: "Audio Synthesis",
		Desc:  "Speak a piece of text",
		Image: "https://nanonets.com/some/image/link",
	}
	OpMetadataMap[ObjectDetection] = OpMetadata{
		Title: "Object Detection",
		Desc:  "Detect an object in the image",
		Image: "https://nanonets.com/some/image/link",
	}
	OpMetadataMap[TextExtraction] = OpMetadata{
		Title: "Text Extraction",
		Desc:  "Extract a piece of text",
		Image: "https://nanonets.com/some/image/link",
	}
	OpMetadataMap[Scoring] = OpMetadata{
		Title: "Scoring",
		Desc:  "Output a number based from a text or an image. Example- angle, size, intensity etc",
		Image: "https://nanonets.com/some/image/link",
	}
	OpMetadataMap[Similarity] = OpMetadata{
		Title: "Similarity detection",
		Desc:  "Figure how similar are two images or two pieces of text",
		Image: "https://nanonets.com/some/image/link",
	}
	OpMetadataMap[Summarization] = OpMetadata{
		Title: "Summarization",
		Desc:  "Summarize a piece of text from a video or an audio",
		Image: "https://nanonets.com/some/image/link",
	}
}

func validateInput(s string) (InputType, error) {
	if s != string(Image) && s != string(Text) && s != string(Audio) && s != string(Video) && s != string(Numerical) {
		return "", errors.New("Bad input type")
	}
	return InputType(s), nil
}

func contains(arr []InputType, inp InputType) bool {
	for _, v := range arr {
		if v == inp {
			return true
		}
	}
	return false
}

func getSuggestedOp(inputs []InputType) []Operation {
	var ret []Operation
	for k, v := range OpMap {
		var exit bool
		for _, mv := range v.Must {
			if !contains(inputs, mv) {
				exit = true
				break
			}
		}
		if exit {
			continue
		}
		for _, mv := range v.Can {
			if contains(inputs, mv) {
				ret = append(ret, k)
				break
			}
		}
	}
	return ret
}

func SuggestionHandler(w http.ResponseWriter, r *http.Request) {
	csInputs := strings.Split(r.FormValue("input"), ",")
	if len(csInputs) == 0 || (csInputs[0] == "") {
		SendError(w, "Bad input", http.StatusBadRequest)
		return
	}

	var inputData []InputType
	for _, v := range csInputs {
		if iv, err := validateInput(v); err != nil {
			SendError(w, "Bad input type", http.StatusBadRequest)
			return
		} else {
			inputData = append(inputData, iv)
		}
	}

	ops := getSuggestedOp(inputData)

	var opsMeta []OpMetadata
	for _, v := range ops {
		opsMeta = append(opsMeta, OpMetadataMap[v])
	}
	SendJson(w, map[string]interface{}{
		"operations": opsMeta,
		"userApps":   "not impl yet", // get users projects from datastore
	})
	return
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Serve HTML page")
}

func UploadHandler(w http.ResponseWriter, r *http.Request) {
	userId, err := auth.GetUserId(r)
	if userId == "" {
		SendError(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	dirPath := "./files/" + userId
	os.Mkdir(dirPath, 0755)

	allFiles := r.MultipartForm.File["file"]
	if len(allFiles) == 0 {
		fmt.Println(r.MultipartForm.File)
		SendError(w, "No files submitted", http.StatusBadRequest)
		return
	}

	for _, f := range allFiles {
		file, err := f.Open()
		defer file.Close()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		dst, err := os.Create(dirPath + "/" + f.Filename)
		defer dst.Close()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		//copy the uploaded file to the destination file
		if _, err := io.Copy(dst, file); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	SendJson(w, "Success")
	return
}
