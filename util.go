package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"
)

func ReadBody(r *http.Request, jsonObject interface{}) error {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	if reflect.TypeOf(jsonObject).Kind() != reflect.Ptr {
		return errors.New("Pointer expected")
	}
	return json.Unmarshal(body, jsonObject)
}

func SendJson(w http.ResponseWriter, data interface{}) {
	jsonResponse, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResponse)
	return
}

func SendError(w http.ResponseWriter, msg string, statusCode int) {
	log.Println(msg)
	http.Error(w, msg, statusCode)
}
