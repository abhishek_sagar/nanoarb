package main

import (
	"context"
	"flag"
	"log"
	"nanoarb/auth"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
)

func main() {
	var wait time.Duration
	flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.Parse()

	auth.Init()
	initVars()

	r := mux.NewRouter()

	r.HandleFunc("/suggest", SuggestionHandler).Methods("POST")
	r.HandleFunc("/upload", UploadHandler).Methods("POST")
	r.HandleFunc("/auth/callback", auth.CallbackHandler)
	r.HandleFunc("/auth/login", auth.LoginHandler)
	r.HandleFunc("/auth/profile", auth.ProfileHandler)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))

	http.Handle("/", r)

	srv := &http.Server{
		Addr:         "0.0.0.0:" + auth.Port,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r, // Pass our instance of gorilla/mux in.
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()

	srv.Shutdown(ctx)
	log.Println("shutting down")
	os.Exit(0)

}
