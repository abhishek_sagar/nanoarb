package auth

import (
	"encoding/gob"
	"errors"
	"net/http"

	"github.com/gorilla/sessions"
)

var (
	Store *sessions.CookieStore
)

func Init() error {
	Store = sessions.NewCookieStore([]byte("something-very-secret"))
	gob.Register(map[string]interface{}{})
	return nil
}

func GetUserId(r *http.Request) (string, error) {
	session, err := Store.Get(r, "auth-session")
	if err != nil {
		return "", err
	}
	profile, ok := session.Values["profile"].(map[string]interface{})
	if !ok {
		return "", errors.New("bad session")
	}

	id, _ := profile["sub"].(string)
	if id == "" {
		return "", errors.New("No user id in session")
	}

	return id, nil
}
